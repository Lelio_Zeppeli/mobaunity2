﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnManager : MonoBehaviour
{
    public int team;

    public float sphereRadius = 10f;
    public RaycastHit[] playerRange;

    public float damage = 10f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Créer une sphère autour du spawn, permettant de soigner les joueurs alliés et infliger des dégats aux ennemies
        playerRange = Physics.SphereCastAll(transform.position, sphereRadius, transform.forward, 1);{}
        foreach (var item in playerRange)
        {
            if(item.collider.gameObject.tag == "Player")
            {
                if (item.collider.gameObject.gameObject.GetComponent<playerScript>().team == this.team)
                {
                    item.collider.gameObject.GetComponent<playerScript>().SpawnRegen();
                }
                else
                {
                    item.collider.gameObject.GetComponent<playerScript>().TakeDamage(damage);
                }
                
            }
        }
    }
    
    private void OnDrawGizmos()
    { 
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, sphereRadius);
      
    }
}
