﻿
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float panSpeed = 20f;
    public float panBorderThickness = 10f;
    public Vector2 panLimit;
    public bool isLocked;
    public float scrollSpeed = 20f;
    public float minY = 20f;
    public float maxY = 120f;
    // Start is called before the first frame update
    public Transform player;
    

    // Update is called once per frame
    void Update()
    { //Déclaration des coté de l'écran avec déplacement de la caméra
        Vector3 pos = transform.position;
        

        if (Input.mousePosition.y >= Screen.height - panBorderThickness) //top
        {
            pos.x -= panSpeed * Time.deltaTime;
            
            
        }

        if (Input.mousePosition.y <= panBorderThickness) //bot
        {
            pos.x += panSpeed * Time.deltaTime;
            
        }

        if (Input.mousePosition.x >= Screen.width - panBorderThickness) //right
        {
            pos.z += panSpeed * Time.deltaTime;

        }

        if (Input.mousePosition.x <= panBorderThickness) //left
        {
            pos.z -= panSpeed * Time.deltaTime;
            
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        pos.y -= scroll * scrollSpeed * 100f * Time.deltaTime;

        pos.x = Mathf.Clamp(pos.x, -panLimit.x, panLimit.x); //limites de deéplacement de la caméra sur l'axe y et z
        pos.z = Mathf.Clamp(pos.z, -panLimit.y, panLimit.y);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);
        transform.position = pos;

        if (Input.GetKey(KeyCode.Space)) //retour de la camera a la position du joueur
        {
            CenterCam();
        }

        

        if (Input.GetKeyDown(KeyCode.A) && isLocked == false) //Lock de la caméra sur le player
        {
            isLocked = true;
        }
        else if (Input.GetKeyDown(KeyCode.A) && isLocked == true)
        {
            isLocked = false;
        }
        if (isLocked)
        {
            CenterCam();
        }

    }

    public void CenterCam() //centre la cam sur le player
    {
        var vec = player.position;
        vec.y = transform.position.y;
        vec.x += 18;
        transform.position = vec;
    }
}

