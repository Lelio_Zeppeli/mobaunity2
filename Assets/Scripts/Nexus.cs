﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Nexus : MonoBehaviour
{
    public Image Victoryscreen;
    public int health;
    public int damage;
    public Slider healthBar;
    // Start is called before the first frame update
    void Start()
    {
        Victoryscreen.enabled = false;
    }

    // Update is called once per frame

    void Update()
    {
        
        
            if (health <= 0)
        {
            Die();
        }
        healthBar.value = health;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GameObject.FindGameObjectWithTag("Bullet"))
        {
            Damage();
        }
    }

    public void Damage()
    {
       // if (GameObject.Find("Tourelle").GetComponent<TourelleSphere>().ravaged == true)

        {
           
            health -= damage;
            healthBar.value = health;
            if (health == 0)
                Die();
            
        }
        
    }


    void Die()
    {
        Time.timeScale = 0f;
        Victoryscreen.enabled = true;
    }

    

}
