﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spellProjectile : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("DestroyAfter1");
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Translate(Vector3.forward *5f);
    }

    IEnumerable DestroyAfter1()
    {
        Debug.Log("1");
        yield return new WaitForSeconds(1.5f);
        Debug.Log("2");
        Destroy(gameObject);
    }
}
