﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript : MonoBehaviour
{
    public GameObject target;
    public GameObject[] bulletPrefabs;
    public playerManager manager;
    public Transform spawn;

    public bool onSpawnRegen = false;
    public float timerSpawnRegen = 1;
    public bool isDead = false;
    public int team;
    
    public float health;
    public float maxHealth;
    
    public float mana;
    public float maxMana;

    public float healthPerSecond;
    public float timerHealth = 1;
    public float manaPerSecond;
    public float timerMana = 1;
    
    public float attack_speed;
    public float attack;
    public float movement_speed;
    public float armor;
    public float magic_resist;
    public int range;

    public int xp_points;
    public int xp_to_lvl_up;
    public int level = 1;

    public float timerMoney = 1;
    public int moneyPer10Seconds = 20;
    public int moneyPlayer;
    
    //BUFFS
    public float timerBuffRed = 120;
    public float timerBuffBlue = 120;
    public bool onRedBuff;
    public bool onBlueBuff;

    public float timerRegenWaterDrake = 5;
    public bool onRegenDrake;
    

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindObjectOfType<playerManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //health management
        if (health <= 0)
        {
            manager.PlayerDead(gameObject.GetComponent<playerScript>()); 
            isDead = true;
        }
        //Mana Management
        if (mana > maxMana)
        {
            mana = maxMana;
        }
        //Life Management
        if (health > maxHealth)
        {
            health = maxHealth;
        }

        if (!isDead)
        {
             //Health Regen
        if (health <= maxHealth)
        {
            if (timerHealth >= 0)
            {
                timerHealth -= Time.deltaTime;
            }
            if (timerHealth <= 0)
            {
                timerHealth = 1;
                health += healthPerSecond;
                
            }
        }
        //Mana Regen
        if (mana <= maxMana)
        {
            if (timerMana >= 0)
            {
                timerMana -= Time.deltaTime;
            }
            if (timerMana <= 0)
            {
                timerMana = 1;
                mana += manaPerSecond;
                
            }
        }
        
        //SpawnRegen
        if (onSpawnRegen)
        {
            if (timerSpawnRegen >= 0)
            {
                timerSpawnRegen -= Time.deltaTime;
                if (timerSpawnRegen <= 0)
                {
                    health += 200;
                    mana += 200;
                    timerSpawnRegen = 1;
                    
                }
            }

            if (health >= maxHealth && mana >= maxMana)
            {
                onSpawnRegen = false;
            }
        }
        //MoneyPerSecond
        if (timerMoney >= 0)
        {
            timerMoney -= Time.deltaTime;
            if (timerMoney <= 0)
            {
                moneyPlayer += moneyPer10Seconds/10;
                timerMoney = 1;
            }
        }
        //Level up
        if (xp_points >= xp_to_lvl_up)
        {
            LevelUp();
        }
        //Redbuff
        if (onRedBuff)
        {
            if (timerBuffRed >= 0)
            {
                timerBuffRed -= Time.deltaTime;
            }

            if (timerBuffRed <= 0)
            {
                onRedBuff = false;
            }
            
            //TODO
            //Dots on auto attacks 
        }
        //BlueBuff
        if (onBlueBuff)
        {
            if (timerBuffBlue >= 0)
            {
                timerBuffBlue -= Time.deltaTime;
                manaPerSecond = 10;
            }

            if (timerBuffBlue <= 0)
            {
                onBlueBuff = false;
            }
        }
        //WaterDrake
        if (onRegenDrake)
        {
            if (timerRegenWaterDrake >= 0)
            {
                timerRegenWaterDrake -= Time.deltaTime;
            }

            if (timerRegenWaterDrake <= 0)
            {
                if (health <= maxHealth)
                {
                    timerRegenWaterDrake = 5;
                    health += (maxHealth-health) * 0.05f;
                }
            }
        }

        /*
        Debug.DrawRay(transform.position,
            transform.TransformDirection(Vector3.forward)*5f + new Vector3(Input.mousePosition.x,0,0)
            ,Color.red);
        //Debug.Log(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.A))
        {
            //Skill Shot
            Debug.Log(Input.mousePosition.ToString());
            Spell1();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            //Dash
            Spell2();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            //Aoe
            Spell3();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            
            Spell4();
        }*/
        
        }
       
    }
    //Level up du joueur
    public void LevelUp()
    {
        maxHealth += 90;
        maxMana += 50;

        health += 90;
        maxMana += 50;

        attack += 5;
        attack_speed *= 1.05f;

        xp_points = 0;
        xp_to_lvl_up = xp_to_lvl_up + 300;

        level += 1;
        armor += 3;
        magic_resist += 3;
    }

    //Fonctions des buffs
    public void InfernoDrake()
    {
        attack = attack * 1.10f;
        AddMoney(25);
    }
    public void MontainDrake()
    {
        armor = armor * 1.06f;
        magic_resist = magic_resist * 1.06f;
        AddMoney(25);
    }
    public void WaterDrake()
    {
        onRegenDrake = true;
        AddMoney(25);
    }
    public void CloudDrake()
    {
        movement_speed = movement_speed * 1.10f;
        AddMoney(25);
    }
    public void BlueBuffKill()
    {
        if (!onBlueBuff)
        {
            onBlueBuff = true;
        }
        else
        {
            timerBuffBlue = 120;
        }
        AddMoney(100);
        AddXP(200);
    }
    public void RedBuffKill()
    {
        if (!onRedBuff)
        {
            onRedBuff = true;
        }
        else
        {
            timerBuffRed = 120;
        }
        AddMoney(100);
        AddXP(200);
    }

    //TODO
    //Fonction appellée quand on tue un sbire 
    public void MinionKill()
    {
        AddMoney(25);
    }
    //Ajout d'xp via la variable
    public void AddXP(int xp)
    {
        xp_points += xp;
    }
    //Ajout d'or via la money
    public void AddMoney(int money)
    {
        moneyPlayer += money;
    }
    //Fonction qui fait revivre le personnage
    public void Respawn()
    {
        if (isDead)
        {
            isDead = false;
        }
        gameObject.SetActive(true);
        transform.position = spawn.position;
        health = maxHealth;
        mana = maxMana;
    }

    //Active la régnène au spawn
    public void SpawnRegen()
    {
        if (!onSpawnRegen)
        {
            onSpawnRegen = true;
        }
    }

    //Inflige des dégats au joueur
    public void TakeDamage(float damage)
    {
        health -= damage;
    }
    
    //Fonctions des spells
    public void Spell1()
    {
        GameObject spellProjectile = Instantiate(bulletPrefabs[0], transform.position, Quaternion.Euler(Input.mousePosition));
        //Debug.DrawRay(transform.position,transform.TransformDirection(Input.mousePosition)*20f,Color.red);
        Debug.Log("Spell1");
    }
    public void Spell2()
    {
        Debug.Log("Spell2");
    }
    public void Spell3()
    {
        Debug.Log("Spell3");
    }
    public void Spell4()
    {
        Debug.Log("Spell4");
    }
    
    
}
