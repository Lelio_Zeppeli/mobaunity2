﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerManager : MonoBehaviour
{

    public Transform spawnPlayer;

    public float timerRespawn = 6;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Durant la mort, il y a un délai avant que le joueur ne respawn
    public IEnumerator Death(playerScript aPlayer)
    {
        aPlayer.gameObject.SetActive(false);
        Debug.Log("Death");
        yield return new WaitForSeconds(timerRespawn + aPlayer.level * 3);
        aPlayer.Respawn();
    }

    //Méthode lancé depuis le script playerScript permettant de lancer la coroutine Death
    public void PlayerDead(playerScript aPlayer)
    {
        //Debug.Log("Death");
        StartCoroutine("Death",aPlayer);
        
    }
}
