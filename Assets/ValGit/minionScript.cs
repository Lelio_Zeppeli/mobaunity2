﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minionScript : MonoBehaviour
{
    public float health;
    public float maxHealth;
    
    public float attack_speed;
    public float attack;
    public float movement_speed;
    public float armor;
    public float magic_resist;
    public int range;
    
    public GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        //Quand un minion (pas encore crée) meurt à coté du player, ce dernier gagne de l'xp
        RaycastHit[] minionInfo = Physics.SphereCastAll(transform.position, 20f, transform.forward, 1);{}
        foreach (var item in minionInfo)
        {
            if(item.collider.gameObject.tag == "Player")
            {
                Debug.Log("Player nearby");
                item.collider.gameObject.GetComponent<playerScript>().AddXP(50);
            }
        }
    }
}
